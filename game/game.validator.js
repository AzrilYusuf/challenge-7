const { body } = require("express-validator");

class GameValidation {
  recordGameVsComValidRules = () => {
    return [
      body("player_choice")
        .notEmpty()
        .withMessage("Please choose your choice")
        .isString()
        .isIn(["rock", "paper", "scissors"])
        .withMessage("Choose between rock, paper or scissors!"),
      body("com_choice").notEmpty().isIn(["rock", "paper", "scissors"]),
    ];
  };

  createRoomValidRules = () => {
    return [
      body("room_name")
        .notEmpty()
        .withMessage("Room name cannot be empty, please input the room name")
        .isString()
        .withMessage("Room name should be letter, alphabet or character")
        .isLength({ min: 2 })
        .withMessage("Room name must be at least 2 characters")
        .isLength({ max: 100 })
        .withMessage("Room name must be maximum 100 characters"),
      body("player1_choice")
        .notEmpty()
        .withMessage("Please choose your choice")
        .isString()
        .isIn(["rock", "paper", "scissors"])
        .withMessage("Choose between rock, paper or scissors!"),
    ];
  };

  recordPlayer2ChoiceValidRules = () => {
    return [
      body("player2_choice")
        .notEmpty()
        .withMessage("Please choose your choice")
        .isString()
        .isIn(["rock", "paper", "scissors"])
        .withMessage("Choose between rock, paper or scissors!"),
    ];
  };
}

module.exports = new GameValidation();
