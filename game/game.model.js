const db = require("../db/models");

class GameModel {
  getGameHistory = async (idUser) => {
    return await db.User_Game.findOne({
      attributes: {
        exclude: ["id", "email", "password", "createdAt", "updatedAt"],
      },
      where: { id: idUser },
      include: [
        {
          model: db.Game_Vs_Com_History,
          as: "player",
        },
        {
          model: db.Game_Room_History,
          as: "player1",
        },
        {
          model: db.Game_Room_History,
          as: "player2",
        },
      ],
    });
  };
  
  getAllDataRooms = async () => {
    return await db.Game_Room_History.findAll({
      raw: true,
      attributes: {
        exclude: [
          "player1_id",
          "player1_choice",
          "result_P1",
          "com_choice",
          "player2_id",
          "player2_choice",
          "result_P2",
          "createdAt",
          "updatedAt",
        ],
      },
      include: [
        {
          model: db.User_Game,
          as: "player1",
          attributes: {
            exclude: ["id", "email", "password", "createdAt", "updatedAt"],
          },
        },
        {
          model: db.User_Game,
          as: "player2",
          attributes: {
            exclude: ["id", "email", "password", "createdAt", "updatedAt"],
          },
        },
      ],
    });
  };

  getRoomDetail = async (idRoom) => {
    return await db.Game_Room_History.findOne({
      raw: true,
      attributes: {
        exclude: [
          "player1_id",
          "player1_choice",
          "result_P1",
          "com_choice",
          "player2_id",
          "player2_choice",
          "result_P2",
          "createdAt",
          "updatedAt",
        ],
      },
      where: { id: idRoom },
      include: [
        {
          model: db.User_Game,
          as: "player1",
          attributes: {
            exclude: ["id", "email", "password", "createdAt", "updatedAt"],
          },
        },
        {
          model: db.User_Game,
          as: "player2",
          attributes: {
            exclude: ["id", "email", "password", "createdAt", "updatedAt"],
          },
        },
      ],
    });
  };

  // Query record game for VS COM
  recordGameVsCom = async (playerId, player_choice, com_choice) => {
    return await db.Game_Vs_Com_History.create({
      player_id: playerId,
      player_choice: player_choice,
      com_choice: com_choice,
    });
  };

  // Result player wins VS COM
  recordResultPlayerWin = async (playerId) => {
    return await db.Game_Vs_Com_History.update(
      {
        result: "win",
      },
      { where: { player_id: playerId } }
    );
  };

  // Result player loses VS COM
  recordResultComWin = async (playerId) => {
    return await db.Game_Vs_Com_History.update(
      {
        result: "lose",
      },
      { where: { player_id: playerId } }
    );
  };

  // Result draw VS COM
  recordResultDrawVsCom = async (playerId) => {
    return await db.Game_Vs_Com_History.update(
      {
        result: "draw",
      },
      { where: { player_id: playerId } }
    );
  };

  isRoomNameUsed = async (room_name) => {
    const namedRoom = await db.Game_Room_History.findOne({
      where: { room_name: room_name },
    });

    if (namedRoom) {
      return true;
    } else {
      return false;
    }
  };

  // Query record game for VS Player 2
  createNewRoom = async (player1Id, room_name, player1_choice) => {
    return await db.Game_Room_History.create({
      player1_id: player1Id,
      room_name: room_name,
      player1_choice: player1_choice,
    });
  };

  getRoomById = async (idRoom) => {
    return await db.Game_Room_History.findOne({ where: { id: idRoom } });
  };

  recordPlayer2Choice = async (idRoom, player2_choice, player2Id) => {
    return await db.Game_Room_History.update(
      {
        player2_id: player2Id,
        player2_choice: player2_choice,
      },
      { where: { id: idRoom } }
    );
  };

  // Result Player 1 wins in a room game
  recordResultPlayer1Win = async (idRoom) => {
    return await db.Game_Room_History.update(
      {
        result_P1: "win",
        result_P2: "lose",
      },
      { where: { id: idRoom } }
    );
  };

  // Result Player 2 wins in a room game
  recordResultPlayer2Win = async (idRoom) => {
    return await db.Game_Room_History.update(
      {
        result_P1: "lose",
        result_P2: "win",
      },
      { where: { id: idRoom } }
    );
  };

  // Result draw in a room game
  recordResultDraw = async (idRoom) => {
    return await db.Game_Room_History.update(
      {
        result_P1: "draw",
        result_P2: "draw",
      },
      { where: { id: idRoom } }
    );
  };
}

module.exports = new GameModel();
