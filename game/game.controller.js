const gameModel = require("./game.model");

class GameController {
  getUserGameHistories = async (req, res) => {
    const { idUser } = req.params;
    try {
      const gameHistories = await gameModel.getGameHistory(idUser);
      if (gameHistories) {
        return res.json(gameHistories);
      } else if (gameHistories === null) {
        res.statusCode = 400;
        return res.json({ message: "Cannot find game history!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ error: error.message });
    }
  };

  getAllRooms = async (req, res) => {
    try {
      const rooms = await gameModel.getAllDataRooms();
      return res.json(rooms);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  };
  
  getOneRoom = async (req, res) => {
    const { idRoom } = req.params;

    try {
      const roomDetail = await gameModel.getRoomDetail(idRoom);

      if (roomDetail) {
        return res.json(roomDetail);
      } else {
        return res
          .status(404)
          .json({ message: `Room with ID: ${idRoom} is not available` });
      }
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  };

  recordGameVsCom = async (req, res) => {
    const playerId = req.token.id;
    const { player_choice, com_choice } = req.body;

    try {
      const recordedGame = await gameModel.recordGameVsCom(playerId, player_choice, com_choice);

      // Verify and input result
      // Player wins
      if (
        (player_choice === "rock" && com_choice === "scissors") ||
        (player_choice === "paper" && com_choice === "rock") ||
        (player_choice === "scissors" && com_choice === "paper")
      ) {
        return (
          res.status(202).json({ message: "You win!" }),
          await gameModel.recordResultPlayerWin(playerId)
        );
      }
      // COM wins
      else if (
        (player_choice === "rock" && com_choice === "paper") ||
        (player_choice === "paper" && com_choice === "scissors") ||
        (player_choice === "scissors" && com_choice === "rock")
      ) {
        return (
          res.status(202).json({ message: "You lose!" }),
          await gameModel.recordResultComWin(playerId)
        );
      }
      // Draw
      else if (
        (player_choice === "rock" && com_choice === "rock") ||
        (player_choice === "paper" && com_choice === "paper") ||
        (player_choice === "scissors" && com_choice === "scissors")
      ) {
        return (
          res.status(202).json({ message: "Draw! No winner!" }),
          await gameModel.recordResultDrawVsCom(playerId)
        );
      }

      return recordedGame;
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  };

  createNewRoom = async (req, res) => {
    const player1Id = req.token.id;
    const { room_name, player1_choice } = req.body;
    // Check is room name already created or not
    const namedRoom = await gameModel.isRoomNameUsed(room_name);
    if (namedRoom) {
      return res.status(409).json({
        message: "The Room Name already in use! Please use another name",
      });
    }

    try {
      // If room name hasn't already been created yet, then create new room
      const createdRoom = await gameModel.createNewRoom(
        player1Id,
        room_name,
        player1_choice
      );

      if (createdRoom) {
        return res.status(201).json({ message: "Success create new room!" });
      }
    } catch (error) {
      return res.status(400).json({ error: error });
    }
  };

  recordPlayer2Choice = async (req, res) => {
    const { idRoom } = req.params;
    const { player2_choice } = req.body;
    const player2Id = req.token.id;

    const roomDetail = await gameModel.getRoomById(idRoom);
    // Verify ID room
    if (roomDetail === null) {
      return res.status(404).json({ message: "Opss.. Something went wrong!" });
    }
    // Check Are player 1 and 2 the same player or not
    if (roomDetail.player1_id === player2Id) {
      return res
        .status(403)
        .json({ message: "You can't play against yourself!" });
    }
    // Check Is a room already played or not yet
    if (roomDetail.result_P1 !== null && roomDetail.result_P2 !== null) {
      return res
        .status(403)
        .json({ message: "The Game can only be played once!" });
    }

    try {
      const recordedChoice = await gameModel.recordPlayer2Choice(
        idRoom,
        player2_choice,
        player2Id
      );
      const player1 = roomDetail.player1_choice;
      const player2 = player2_choice;

      // Verify and input result
      // Player 1 wins
      if (
        (player1 === "rock" && player2 === "scissors") ||
        (player1 === "paper" && player2 === "rock") ||
        (player1 === "scissors" && player2 === "paper")
      ) {
        return (
          res.status(202).json({ message: "You lose!" }),
          await gameModel.recordResultPlayer1Win(idRoom)
        );
      }
      // Player 2 wins
      else if (
        (player1 === "rock" && player2 === "paper") ||
        (player1 === "paper" && player2 === "scissors") ||
        (player1 === "scissors" && player2 === "rock")
      ) {
        return (
          res.status(202).json({ message: "You win!" }),
          await gameModel.recordResultPlayer2Win(idRoom)
        );
      }
      // Draw
      else if (
        (player1 === "rock" && player2 === "rock") ||
        (player1 === "paper" && player2 === "paper") ||
        (player1 === "scissors" && player2 === "scissors")
      ) {
        return (
          res.status(202).json({ message: "Draw! No winner!" }),
          await gameModel.recordResultDraw(idRoom)
        );
      }

      return recordedChoice;
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  };
}

module.exports = new GameController();
