const express = require("express");
const gameRouter = express.Router();
const gameController = require("./game.controller");
const authorization = require("../middleware/authMiddleware");
const authProtection = require("../middleware/protectionMiddleware");
const gameValidation = require("./game.validator");
const schemaValidation = require("../middleware/schemaValidation");

// API to Get user's game history by ID
gameRouter.get(
  "/history/:idUser",
  authorization,
  authProtection,
  gameController.getUserGameHistories
);

// API to Get all rooms
gameRouter.get("/rooms", authorization, gameController.getAllRooms);

// API to Get one room
gameRouter.get("/rooms/:idRoom", authorization, gameController.getOneRoom);

// API to Record game history VS COM
gameRouter.post(
  "/",
  authorization,
  gameValidation.recordGameVsComValidRules(),
  schemaValidation,
  gameController.recordGameVsCom
);

// API to Create room VS Player 2
gameRouter.post(
  "/create-room",
  authorization,
  gameValidation.createRoomValidRules(),
  schemaValidation,
  gameController.createNewRoom
);

// API to Record player 2's choice
gameRouter.put(
  "/rooms/:idRoom",
  authorization,
  gameValidation.recordPlayer2ChoiceValidRules(),
  schemaValidation,
  gameController.recordPlayer2Choice
);

module.exports = gameRouter;
