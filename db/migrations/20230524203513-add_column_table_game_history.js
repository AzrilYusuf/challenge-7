"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    // Add column
    await queryInterface.addColumn("User_Game_Histories", "room_name", {
      type: Sequelize.STRING(100),
      allowNull: false,
    });
    await queryInterface.addColumn("User_Game_Histories", "player2_id", {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: "User_Games",
        key: "id",
      },
    });
    await queryInterface.addColumn("User_Game_Histories", "player2_choice", {
      type: Sequelize.STRING(100),
      allowNull: true,
    });
    await queryInterface.addColumn("User_Game_Histories", "result_P2", {
      type: Sequelize.STRING(100),
      allowNull: true,
    });

    // Change attribute
    await queryInterface.changeColumn("User_Game_Histories", "com_choice", {
      type: Sequelize.STRING(100),
      allowNull: true,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
