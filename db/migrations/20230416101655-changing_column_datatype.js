"use strict";

const { DataTypes } = require("sequelize");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn("User_Games", "username", {
      type: DataTypes.STRING(100),
      allowNull: false,
    });

    await queryInterface.changeColumn("User_Games", "email", {
      type: DataTypes.STRING,
      allowNull: false,
    });

    await queryInterface.changeColumn("User_Games", "password", {
      type: DataTypes.STRING,
      allowNull: false,
    });

    await queryInterface.changeColumn("User_Game_Biodata", "fullname", {
      type: DataTypes.STRING,
      allowNull: false,
    });

    await queryInterface.changeColumn("User_Game_Biodata", "user_id", {
      type: DataTypes.INTEGER,
      allowNull: false,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
