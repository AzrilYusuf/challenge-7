'use strict';

const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeColumn('User_Game_Histories', 'player2_choice');

    await queryInterface.removeColumn('User_Game_Histories', 'result_P2');
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.addColumn("User_Game_Histories", "player2_choice", {
      type: Sequelize.STRING(100),
      allowNull: true,
    });

    await queryInterface.addColumn("User_Game_Histories", "result_P2", {
      type: Sequelize.STRING(100),
      allowNull: true,
    });
  }
};
