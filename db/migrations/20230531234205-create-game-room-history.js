"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Game_Room_Histories", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      room_name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      player1_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player1_choice: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      result_P1: {
        type: Sequelize.STRING(100),
      },
      player2_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player2_choice: {
        type: Sequelize.STRING(100),
      },
      result_P2: {
        type: Sequelize.STRING(100),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Game_Room_Histories");
  },
};
