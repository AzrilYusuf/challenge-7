"use strict";

const { DataTypes } = require("sequelize");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn("User_Game_Biodata", "date_of_birth", {
      type: DataTypes.STRING(100),
      allowNull: true,
    });

    await queryInterface.changeColumn("User_Game_Biodata", "address", {
      type: DataTypes.STRING,
      allowNull: true,
    });

    await queryInterface.changeColumn("User_Game_Biodata", "phone_number", {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
