"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Game_Room_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Game_Room_History.belongsTo(models.User_Game, {
        foreignKey: "player1_id",
        as: "player1",
      });
      Game_Room_History.belongsTo(models.User_Game, {
        foreignKey: "player2_id",
        as: "player2",
      });
    }
  }
  Game_Room_History.init(
    {
      room_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      player1_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player1_choice: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      result_P1: {
        type: DataTypes.STRING(100),
      },
      player2_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player2_choice: {
        type: DataTypes.STRING(100),
      },
      result_P2: {
        type: DataTypes.STRING(100),
      },
    },
    {
      sequelize,
      modelName: "Game_Room_History",
    }
  );
  return Game_Room_History;
};
