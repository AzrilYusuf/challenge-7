"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Game_Vs_Com_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Game_Vs_Com_History.belongsTo(models.User_Game, {
        foreignKey: "player_id",
        as: "player"
      });
    }
  }
  Game_Vs_Com_History.init(
    {
      player_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player_choice: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      com_choice: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      result: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "Game_Vs_Com_History",
    }
  );
  return Game_Vs_Com_History;
};
