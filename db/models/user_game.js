"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_Game.hasOne(models.User_Game_Biodata, { foreignKey: "user_id" });
      User_Game.hasMany(models.Game_Vs_Com_History, { foreignKey: "player_id", as: "player" });
      User_Game.hasMany(models.Game_Room_History, { foreignKey: "player1_id", as: "player1" });
      User_Game.hasMany(models.Game_Room_History, { foreignKey: "player2_id", as: "player2" });
    }
  }
  User_Game.init(
    {
      username: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "User_Game",
    }
  );
  return User_Game;
};
