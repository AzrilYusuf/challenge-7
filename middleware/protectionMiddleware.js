const authProtection = async (req, res, next) => {
    const tokenId = req.token.id.toString();
    const { idUser } = req.params;

    if ((tokenId !== idUser)) {
        return res.status(401).json({ message: "Invalid token!"})
    } else if ((tokenId === idUser)) {
        next();
    }
};

module.exports = authProtection;