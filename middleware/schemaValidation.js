const { validationResult } = require("express-validator");

const schemaValidation = async (req, res, next) => {
    const result = validationResult(req);

    if (result.isEmpty()) {
        next();
    } else {
        res.statusCode = 400;
        return res.json({ errors: { [result.array()[0].path]: result.array()[0].msg } });
    }
}

module.exports = schemaValidation;