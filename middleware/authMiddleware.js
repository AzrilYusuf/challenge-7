const JWT = require("jsonwebtoken");

const authorization = async (req, res, next) => {
  // Get authorization key in header
  const { authorization } = req.headers;

  // Check is there authorization in header or not
  if (authorization === undefined) {
    res.statusCode = 401;
    return res.json({ message: "Unauthorized! Cannot use this method!" });
  }

  try {
    const splitedToken = authorization.split("")[1]
    // Verify if authorization key is valid
    const token = await JWT.verify(
      splitedToken,
      "q4t7w!z%C*F-JaNdRgUkXp2r5u8x/A?D(G+KbPeShVmYq3t6v9y$B&E)H@McQfTj"
    );
    req.token = token;
    next();
  } catch (error) {
    res.statusCode = 401;
    return res.json({ message: "Invalid token!" });
  }
};

module.exports = authorization;
