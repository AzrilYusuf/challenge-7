const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const authorization = require("../middleware/authMiddleware");
const authProtection = require("../middleware/protectionMiddleware");
const userValidation = require("./users.validator");
const schemaValidation = require("../middleware/schemaValidation");

// API to Get data users
userRouter.get("/", authorization, userController.getAllUsers);

// API to Sign Up/Register
userRouter.post(
  "/signup",
  userValidation.registrationValidRules(),
  schemaValidation,
  userController.userSignUp
);

// API to Login/Sign In
userRouter.post(
  "/login",
  userValidation.loginValidRules(),
  schemaValidation,
  userController.userLogin
);

// API to Update and Insert biodata users
userRouter.put(
  "/biodata/:idUser",
  authorization,
  authProtection,
  userValidation.updateBioValidRules(),
  schemaValidation,
  userController.updateUserBiodata
);

// API to Get user's biodata by ID
userRouter.get(
  "/biodata/:idUser",
  authorization,
  authProtection,
  userController.getDetailUser
);

module.exports = userRouter;
