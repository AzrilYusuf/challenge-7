const userModel = require("./users.model");
const JWT = require("jsonwebtoken");

class UserController {
  // Get data users
  getAllUsers = async (req, res) => {
    try {
      const allUsers = await userModel.getAllUsers();
      return res.json(allUsers);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  };

  // Sign Up
  userSignUp = async (req, res) => {
    const requestedData = req.body;

    // Check have username and email already been registered
    const registeredUser = await userModel.isUserRegistered(requestedData);
    if (registeredUser) {
      res.statusCode = 409;
      return res.json({
        message:
          "This username or email is exist! Please use another username or email.",
      });
    }
    // Add new data user
    await userModel.createNewUser(requestedData);
    return res.status(201).json({ message: "New user has been registered!" });
  };

  // login
  userLogin = async (req, res) => {
    const { username, password } = req.body;

    try {
      const dataUserLogin = await userModel.verifyLogin(username, password);
      // delete dataUserLogin.password;

      // Check is data valid or invalid
      if (dataUserLogin) {
        // Generate JWT
        const token = JWT.sign(
          { ...dataUserLogin, role: "player" },
          "q4t7w!z%C*F-JaNdRgUkXp2r5u8x/A?D(G+KbPeShVmYq3t6v9y$B&E)H@McQfTj",
          { expiresIn: "1h" }
        );

        return res.json({ accessToken: token });
      } else {
        res.statusCode = 400;
        return res.json({ message: "Username or password is invalid!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ message: "User is not found! Something went wrong." });
    }
  };

  updateUserBiodata = async (req, res) => {
    const { idUser } = req.params;
    const requestedDataBio = req.body;

    await userModel.upsertUserBiodata(idUser, requestedDataBio);
    return res.json({ message: "User biodata updated!" });
  };

  getDetailUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const detailUser = await userModel.getBiodataUser(idUser);
      if (detailUser) {
        return res.json(detailUser);
      } else {
        res.statusCode = 400;
        return res.json({ message: "User biodata is not found!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ message: "Opss.. Something went wrong!" });
    }
  };
}

module.exports = new UserController();
