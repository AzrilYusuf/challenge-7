const { body } = require("express-validator");

class UserValidation {
  registrationValidRules = () => {
    return [
      body("username")
        .notEmpty()
        .withMessage("Please input your username")
        .isString()
        .withMessage("Username should be letter, alphabet or character")
        .isLength({ min: 4 })
        .withMessage("Username must be at least 4 characters"),
      body("email")
        .notEmpty()
        .withMessage("Please input your email")
        .isEmail()
        .withMessage("Invalid email format, please use valid email"),
      body("password")
        .notEmpty()
        .withMessage("Please input your password")
        .isLength({ min: 8 })
        .withMessage("Password must be at least 8 characters"),
    ];
  };

  loginValidRules = () => {
    return [
      body("username").notEmpty().withMessage("Please input your username"),
      body("password").notEmpty().withMessage("Please input your password"),
    ];
  };

  updateBioValidRules = () => {
    return [
      body("fullname")
        .optional({ nullable: true })
        .isString()
        .withMessage("Fullname must be letter or alphabet"),
      body("date_of_birth")
        .optional({ nullable: true })
        .isDate("dd-mm-yyyy")
        .withMessage("Invalid format, input must use: dd-mm-yyyy format"),
      body("address")
        .optional({ nullable: true })
        .isString()
        .withMessage("Address must be letter or alphabet"),
      body("phone_number")
        .optional({ nullable: true })
        .isMobilePhone("id-ID")
        .withMessage("Invalid phone number, please use valid phone number"),
    ];
  };
}

module.exports = new UserValidation();
