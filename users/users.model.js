const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

class UserModel {
  getAllUsers = async () => {
    // SELECT * FROM "Users";
    const dataUsers = await db.User_Game.findAll({
      include: [db.User_Game_Biodata],
    });
    return dataUsers;
  };

  isUserRegistered = async (requestedData) => {
    const existData = await db.User_Game.findOne({
      where: {
        [Op.or]: [
          { username: requestedData.username },
          { email: requestedData.email },
        ],
      },
    });

    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  createNewUser = async (requestedData) => {
    // INSERT INTO TABLE (Users) VALUES ();
    await db.User_Game.create({
      username: requestedData.username,
      email: requestedData.email,
      // Password hashed with md5
      password: md5(requestedData.password),
    });
  };

  verifyLogin = async (username, password) => {
    const dataUser = await db.User_Game.findOne({
      where: { username: username, password: md5(password) },
      attributes: { exclude: ["password"] },
      raw: true,
    });
    return dataUser;
  };

  upsertUserBiodata = async (idUser, requestedDataBio) => {
    return await db.User_Game_Biodata.upsert(
      {
        fullname: requestedDataBio.fullname,
        date_of_birth: requestedDataBio.date_of_birth,
        address: requestedDataBio.address,
        phone_number: requestedDataBio.phone_number,
        user_id: idUser,
      },
      { where: { user_id: idUser } }
    );
  };

  getBiodataUser = async (idUser) => {
    return await db.User_Game_Biodata.findOne({
      where: { user_id: idUser },
      include: [db.User_Game],
    });
  };
}

module.exports = new UserModel();
